package testrunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)
@CucumberOptions(
		features="src/test/java/features",
		glue={"stepdef"},
		dryRun = false,
		monochrome = true,
		tags = "@CS",
		plugin = {"pretty","html:results/html/Sh_Regression.html","json:target/cucumber-reports/cucumber.json"}
		//publish = true
)
public class TestRunner {




}
