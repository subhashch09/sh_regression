Feature: Test CS Support API

	@CS
	Scenario: Test CS Support API
	    Given User calls "CsSupportAPI" with "GET" http request
	    Then Status code is 200
	    And "supportPhoneNumber" in response body is "1-888-757-4774"
		And "tmSupportPhoneNumber" in response body is "1-888-551-7600"