package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class PropertiesReader {
	FileInputStream fis = null;
	Properties pro = null;
	String filepath = System.getProperty("user.dir")+"/config.properties";
	
	public PropertiesReader() {	
		try {

			fis = new FileInputStream(filepath);
			pro = new Properties();
			pro.load(fis);
		} catch (FileNotFoundException fnfe) {
			fnfe.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		} 
	}
	
	public String getPropValue(String keyvalue) {
		return pro.getProperty(keyvalue);
	}

	public String getReportConfigPath(){
		String reportConfigPath = pro.getProperty("reportConfigPath");
		if(reportConfigPath!= null) return reportConfigPath;
		else throw new RuntimeException("Report Config Path not specified in the Configuration.properties file for the Key:reportConfigPath");
	}

}
