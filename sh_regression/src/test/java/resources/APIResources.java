package resources;

public enum APIResources {
	CsSupportAPI("http://10.0.1.4:30383/api/cs/support"),
	TurboAPI("http://10.0.1.4:30383/shipping/local/bound/TURBO"),
	CreateAccountAPI("https://20.49.51.36:8383//party/createAccount");

	private String resource;
	
	APIResources(String resource) {
		this.resource=resource;
	}
	
	public String getResource() {
		return resource;
	}
}
