package base;


import java.io.FileNotFoundException;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import resources.APIResources;
import resources.Payload;



public class CrudOperation {
    static Response response;
    RequestSpecification reqspec;
    String basePathData;

    public Response performPOSTcall(String apiName) throws FileNotFoundException {
        APIResources resourceAPI = APIResources.valueOf(apiName);
        basePathData = resourceAPI.getResource();
        reqspec = new BaseBuilder().placeSpecBuilder();

        System.out.println("Sending POST request to: " + resourceAPI.getResource() + " service");
        reqspec = RestAssured.given().relaxedHTTPSValidation().spec(reqspec).body(Payload.addPayload(""));

        response = reqspec.post(resourceAPI.getResource()).then().extract().response();
        return response;
    }

    public Response performGETcall(String apiName) throws FileNotFoundException {
        APIResources resourceAPI = APIResources.valueOf(apiName);
        reqspec = new BaseBuilder().placeSpecBuilder();
        System.out.println("Sending GET request to: " + resourceAPI.getResource() + " service");
        response = RestAssured.given().relaxedHTTPSValidation().spec(reqspec).get(resourceAPI.getResource());
        return response;
    }

}
