package base;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utilities.PropertiesReader;

public class BaseBuilder {
	PropertiesReader pro = new PropertiesReader();
	RequestSpecBuilder builder;
	RequestSpecification reqspec;
	Response response;
	PrintStream log;
	
	public RequestSpecification placeSpecBuilder() throws FileNotFoundException {
		builder = new RequestSpecBuilder();
		

		String env="QA";

		
		if(env.equals("QA")) {
			System.out.println("Running the Test Scripts on QA Environment"+pro.getPropValue("QA"));
			builder.setBaseUri(pro.getPropValue("QA"));

		} 
		else if(env.equalsIgnoreCase("DEV")) {
			System.out.println("Running the Test Scripts on QA Environment"+pro.getPropValue("DEV"));
			builder.setBaseUri(pro.getPropValue("DEV"));
		}
		else { 
			builder.setBaseUri(pro.getPropValue("QA"));
		}
		System.out.println("Building Headers....");
		builder.setContentType("application/json");
		log = new PrintStream(new FileOutputStream("log.txt"));
		builder.addFilter(RequestLoggingFilter.logRequestTo(log));
		builder.addFilter(ResponseLoggingFilter.logResponseTo(log));
		reqspec =  builder.build();	
		return reqspec;
	}
}
